const express = require('express');
const router = express.Router();

const multer = require('../middleware/multer-config');
const collectionCtrl = require('../controllers/collection');
const questionCtrl = require('../controllers/question');

router.get('/', collectionCtrl.getAll);
router.post('/', collectionCtrl.createOne);
router.delete('/:id', collectionCtrl.deleteOne);

router.post('/:id/question/', multer, questionCtrl.createOne);
router.put('/:id/question/', multer, questionCtrl.modifyOne);

module.exports = router;
