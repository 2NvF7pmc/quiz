const express = require('express');
const router = express.Router();

const seriesCtrl = require('../controllers/series');

router.get('/:id', seriesCtrl.generatOne)

module.exports = router;
