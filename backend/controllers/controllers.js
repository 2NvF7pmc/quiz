// The server could not understand the request due to invalid syntax.
exports.badRequest = (e, res) => res.status(400).json({ e });

// The server can not find the requested resource.
exports.notFound = (e, res) => res.status(404).json({ e });

//The server could not understand the request due to invalid syntax.
exports.badRequest = (e, res) => res.status(400).json({ e });

//The server has encountered a situation it doesn't know how to handle.
exports.internalServerError = (e, res) => res.status(500).json({ e });
