const fs = require('fs');
const controllers = require('./controllers');

const Collection = require('../models/Collection');
const Question = require('../models/Question');
const Answer = require('../models/Answers');

exports.getAll = (req, res, next) => {
    Collection.find()
        .then(col => res.status(200).json(col))
        .catch(e => controllers.badRequest(e, res));
}

exports.createOne = (req, res, next) => {

    const newObject = new Collection({
        ...req.body,
        // questions = new Question(),
        // answers = new Answer()
    })

    newObject.save()
        .then(() => res.status(201).json({ msg: 'collection create'}))
        .catch(e => controllers.notFound(e, res));
}

exports.deleteOne = (req, res, next) => {

    const targetObject = { _id: req.params.id};

    Collection.findOne(targetObject)
        .then(col => {

            if(col.questions)
            {
                col.questions.forEach((v, k) => {
                    const filename = v.imageURL.split('/public/images/')[1];
                    fs.unlink(`public/images/${filename}`);
                });
            }

            Collection.deleteOne(targetObject)
                .then(() => res.status(200).json({ msg: 'collection suprimer'}))
                .catch(e => controllers.badRequest(e, res));
        })
        .catch(e => controllers.internalServerError(e, res));
}
