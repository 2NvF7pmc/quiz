const mongoose = require('mongoose');

const answersSchema = mongoose.Schema({
    questionId: {type: String, required: true},
    answersId: [Number]
});

//module.exports = mongoose.model('Answers', answersSchema);
module.exports = answersSchema;
