const mongoose = require('mongoose');

const Question = require('./Question');
const Answer = require('./Answers');

const collectionSchema = mongoose.Schema({
    displayName: { type: String, required: true },
    description: { type: String, default: null },
    questions: {type: [Question], default: undefined},
    answers: {type: [Answer], default: undefined}
});

module.exports = mongoose.model('Collection', collectionSchema);
