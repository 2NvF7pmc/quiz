const mongoose = require('mongoose');

const seriesSchema = mongoose.Schema({
    seed: {type: Number, required: true},
    startedOn: {type: Number, required: true},
    myAnswers: [Number]
});

module.exports = mongoose.Model('Series', seriesSchema);
