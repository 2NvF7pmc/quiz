import PropType from "prop-types"
import React, {Component} from "react"
import './Results.css'

function AnswerResult({answers, myAnswersSet, answersSet}) {
    return (<ul className="answer-container">
        {answers.map((item, i) => (
            <li
                key={i}
                className={`answer-container ${
                    myAnswersSet.has(i)
                    && (answersSet.has(i) ? 'correct-answer' : 'bad-answer')
                }`}>
                {item}</li>))}
    </ul>)
}

class Results extends Component{

    render() {
        const {data:{answers}, historyAnswer, answerData} = this.props
        return(<>
            <div className="results-container">
                {answers.map((item, i) => {
                    const {img, question, answers} = item
                    return (<div key={i} className="result-item">
                        <div className="left-item">
                            <img src={process.env.PUBLIC_URL + img} alt=""/>
                        </div>
                        <div className="right-item">
                            <h5>{question}</h5>
                            <AnswerResult
                                answers={answers}
                                myAnswersSet={historyAnswer[i]}
                                answersSet={new Set(answerData.answers[i].answer)}/>
                        </div>
                    </div>)
                })}
            </div>
        </>)
    }
}

Results.propTypes = {
    historyAnswer: PropType.array.isRequired,
    data: PropType.arrayOf({
        answers: PropType.shape({
            id: PropType.number.isRequired,
            img: PropType.string.isRequired,
            question: PropType.string.isRequired,
            answers: PropType.array.isRequired
        })
    }).isRequired,
    answerData: PropType.arrayOf({
        id: PropType.number.isRequired,
        answer: PropType.array.isRequired
    })
}

export default Results
