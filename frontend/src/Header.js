import React, {Component, useState} from 'react';
import './Header.css';

function NavButton ({ name, id, currentIndex }) {
    return(<li className={`nav-item ${currentIndex === id && 'active'}`}>
        <a data-index={id} href="#">{name}</a>
    </li>)
}

class Header extends Component{

    state = {
        index: this.props.index || 0
    }

    _handleIndexChange = (e) => {
        const targetIndex =
            parseInt(e.target.getAttribute('data-index'))  || 0;

        this.setState({
            index: targetIndex
        });
    }

    render() {

        const { index } = this.state;

        return(<header className="header" onClick={this._handleIndexChange}>
            <div>
                <nav className="nav">
                    <ul >
                        <NavButton name="Acceuil" id={0} currentIndex={index}/>
                        <NavButton name="Series" id={1} currentIndex={index}/>
                        {/*<NavButton name="Mon historie" id={2} currentIndex={index}/>*/}
                    </ul>
                </nav>
            </div>
            <div className="nav">
                <ul>
                    <NavButton name="Retour" id={2} currentIndex={index}/>
                    {/*<NavButton name="Se connecter" id={3} currentIndex={index}/>*/}
                    {/*<NavButton name="S'inscrire" id={4} currentIndex={index}/>*/}
                </ul>
            </div>
        </header>);
    }
}

export default Header;
