import PropType from "prop-types"
import React, {Component} from "react"
import './Question.css'

import ButtonGroup from "./Forms/ButtonGroup"

class Question extends Component{

    state = {
        timers: this.props.startTime,
        canSelect: true,
        currentId: this.props.items.id
    }

    static getDerivedStateFromProps(props, state){
        if(props.items.id !== state.currentId)
        {
            return {timers: props.startTime,
                canSelect: true,
                currentId: props.items.id}
        }
        return null
    }

    componentDidMount() {
        this.interval = setInterval(() => {
            this.setState((s) => {

                let newTime = s.timers

                if (newTime > 0) {
                    newTime--
                    return {timers: newTime}
                } else {
                    //TODO : Resquest answer from API
                    return {
                        timers: newTime,
                        canSelect: false
                    }
                }
            })
        }, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    render() {
        const {items: {img, question, answers}, nextQuestion, answersSet, handleOnSelect} = this.props
        const {timers, canSelect} = this.state

        return (<>
            <div className="head-container">
                <img src={process.env.PUBLIC_URL + img} alt=""/>
                {(timers === 0 || answersSet.size > 0) &&
                (<button
                    className="next-btn"
                    onClick={() => nextQuestion([])}>Suivant</button>)}
            </div>
            <div className="answer-container">
                <div className="answer-header">
                    <h3>{question}</h3>
                    <p>{timers}</p>
                </div>
                <ButtonGroup
                    items={answers}
                    isMultiple
                    canSelect={canSelect}
                    answersSet={answersSet}
                    handleOnSelect={handleOnSelect}/>
            </div>
        </>)
    }
}

Question.defaultProps = {
    startTime: 5
}

ButtonGroup.propTypes = {
    items: PropType.shape({
        id: PropType.number.isRequired,
        img: PropType.string.isRequired,
        question: PropType.string.isRequired,
        answers: PropType.array.isRequired
    }).isRequired,
    nextQuestion: PropType.func.isRequired/*,
    answersSet,
    handleOnSelect*/
}

// export const QUESTION_TYPE = {
//     MULTIPLE : 0,
//     UNIQUE: 1,
//     YES_NO: 2
// }

export default Question
