import PropType from "prop-types"
import React, {Component} from "react";
import './ButtonGroup.css'

function Button({currentIndex, index, children, onClick, isLocked}) {
    const isSelected = (currentIndex.size > 0 && currentIndex.has(index))

    return (
        (isLocked) ?
            <li className={`btn-locked ${isSelected && 'selected'}`}>
                <p>{children}</p>
            </li> :
            <li className={`btn ${isSelected && 'selected'}`}
                onClick={(e) => onClick(e, index)}>
                <p>{children}</p>
            </li>
    )
}

Button.propTypes = {
    currentIndex: PropType.array.isRequired,
    index: PropType.number.isRequired,
    onClick: PropType.func.isRequired,
    children: PropType.string
}

class ButtonGroup extends Component{
    render() {
        const {buttons, items, canSelect, handleOnSelect, answersSet} = this.props

        return (<div className="btn-container">
            <ul className="btn-list">
                {items.map((v, i) => {

                    const text = `${(buttons.length >= items.length) ? buttons[i] + " - " : ""}${v}`

                    return (<Button
                        currentIndex={answersSet}
                        key={i}
                        index={i}
                        isLocked={!canSelect}
                        onClick={handleOnSelect/*this.handleOnClick*/}>{text}
                    </Button>)
                })}
            </ul>
        </div>)
    }
}

ButtonGroup.defaultProps = {
    buttons: ['A','B','C','D','E', 'F'],
    isMultiple: false,
    canSelect: true
}

ButtonGroup.propTypes = {
    buttons: PropType.array,
    isMultiple: PropType.bool,
    items: PropType.array.isRequired,
    canSelect: PropType.bool/*,
    answersSet: PropType.
    handleOnSelect: PropType.func*/
}

export default ButtonGroup
