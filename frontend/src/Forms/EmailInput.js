import PropType from "prop-types"
import React, {Component} from 'react';

class EmailInput extends Component {

    constructor(props) {
        super(props);
    }

    handleOnValueChange = ({target : {value}}) => {
        const { schema, valueChange } = this.props

        let re = new RegExp(schema, "ig")

        const inputValue = value.toString().trim()

        valueChange(inputValue, re.test(inputValue))
    }

    render() {
        const {name, value} = this.props
        return (
            <div>
                <div>
                    <label htmlFor={name}>Email</label>
                    <input type="text" name={name} id={name} value={value} onChange={this.handleOnValueChange}/>
                </div>
            </div>
        )
    }
}

EmailInput.defaultProps ={
    schema: '[a-z]*@[a-z]*'
}

EmailInput.propTypes = {
    name: PropType.string.isRequired,
    value: PropType.string,
    valueChange: PropType.func.isRequired,
    schema: PropType.string
}

export default EmailInput